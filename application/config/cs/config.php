<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
    -------------------------------------
        DATABASE CONNECTION CONFIGURATION
    -------------------------------------
*/

$config['hostname'] = "199.233.238.88:3306";
$config['username'] = "sa";
$config['password'] = "Zxc!!!zxc";

/*
    --------------------------------------
        DATABASE NAMES CONFIGURATION
    --------------------------------------
        WARNING PLEASE CHANGE THIS WITH CAUTION
        CHANGING THIS MAY LEAD TO A MASSIVE ERROR.

    VARIABLE EXPLANATION
        RanCP - Name of your RanCP Database
        RanGame - Name of your RanGame Database
        RanShop - Name of your RanShop Database
        RanUser - Name of your RanUser Database

        and if you're dealing with two or more servers.
        RanGame2 - Name of your another RanGame Database
        * If you're going to add another server please inform
        the developer to make some adjustment to the files.

*/

$config['RanCP'] = "RanCP";
$config['RanGame'] = "BalikGame";
$config['RanShop'] = "BalikShop";
$config['RanUser'] = "BalikUser";
$config['RanGame2'] = "BalikGameHavoc";

/*
    ---------------------------------------
        WEBSITE CONFIGURATION
    ---------------------------------------
*/

$config['server_name'] = "Balik Ran Online";
$config['vs_message'] = "The server is not responsible in any way of purchase mistakes.<br/>Make sure to Verify the right item before buying.<br/>Abosolutely NO Refund, NO Exchange.";
$config['point_name'] = "E-Points";
$config['vpoint_name'] = "V-Points";

/*
    ---------------------------------------
        E-MAIL CONFIGURATION
    ---------------------------------------
*/

$config['email']['default']['protocol'] = "smtp";
$config['email']['default']['smtp_host'] = "ssl://smtp.googlemail.com";
$config['email']['default']['smtp_port'] = 465;
$config['email']['default']['smtp_user'] = "hello.balikran@gmail.com";
$config['email']['default']['smtp_pass'] = "h3llobalikran";
$config['email']['default']['mailtype'] = "html";
$config['email']['default']['wordwrap'] = true;
$config['email']['default']['charset'] = 'iso-8859-1';

$email['email']['active'] = "default";


/*
    ---------------------------------------
        FEE & CONVERTION CONFIGURATION
    ---------------------------------------
    VARIABLE EXPLANATION

    gt_to_vp -  The value of this is equivalent to minutes so the formula will be
                {current_gametime}/gt_to_vp = 1 vp

    ep_to_vp -  This works as a multiplier. Ex. the value is 10
                it will be 1 EP = 10 VP

    change_school - value array({fee}, {type})
                    types :
                        1 = E-Points
                        2 = V-Points
*/

$config['gt_to_vp'] = 90;
$config['ep_to_vp'] = 2;
$config['change_school'] = array(400, 1);
