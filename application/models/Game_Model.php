<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Game_Model extends CI_Model {
    var $dbn = "";
    var $dbn2 = "";

    function __construct() {
        $ci =& get_instance();
        $ci->config->load('cs/config');
        $this->dbn = $ci->config->item('RanGame');
        $this->dbn2 = $ci->config->item('RanGame2');
    }

    function get_characters($UserNum) {
        return $this->db->get_where($this->dbn . '.ChaInfo' , array("UserNum" => $UserNum))->result();
    }

    function update_chainfo($chanum, $data) {
        $this->db->where('ChaNum', $chanum);
        $this->db->update($this->dbn . '.ChaInfo', $data);
        return $this->db->affected_rows();
    }

    function club_rank() {
        $this->db->select('*');
        $this->db->from($this->dbn . '.GuildRegion as a');
        $this->db->join($this->dbn . '.GuildInfo as b' , 'a.GuNum = b.GuNum');
        return $this->db->get()->result();
    }

    function club_member($GuNum) {
        return $this->db->get_where($this->dbn . '.ChaInfo', array('GuNum' => $GuNum))->result();
    }

    function ChaInfo($ChaNum) {
        return $this->db->get_where($this->dbn . '.ChaInfo', array("ChaNum" => $ChaNum))->row();
    }

    function get_guild($GuNum) {
        return $this->db->get_where($this->dbn . '.GuildInfo', array('GuNum' => $GuNum))->row();
    }

    function set_dbn($dbn) {
        $this->dbn = $dbn;
    }

}
