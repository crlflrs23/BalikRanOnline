<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CP_Model extends CI_Model {
    var $dbn = "";

    function __construct() {
        $ci =& get_instance();
        $ci->config->load('cs/config');
        $this->dbn = $ci->config->item('RanCP');
    }

    function get_code($code) {
        return $this->db->get_where($this->dbn . '.Topup', array("Code" => $code))->row();
    }

    function get_points($username) {
        return $this->db->get_where($this->dbn . '.Points', array("UserName" => $username))->row();
    }

    function get_topup() {
        return $this->db->get($this->dbn . '.Topup')->result();
    }


    function insert_points($data) {
        $this->db->insert($this->dbn. '.Points', $data);
        return $this->db->insert_id();
    }

    function check_topup($code, $pin) {
        return $this->db->get_where($this->dbn . '.Topup', array("Code" => $code, "Pin" => $pin))->row();
    }

    function delete_topup($code, $pin) {
        $this->db->where('Code' , $code);
        $this->db->where('Pin', $pin);
        $this->db->delete($this->dbn . '..Topup');
        return $this->db->affected_rows();
    }

    function update_points($data, $username) {
        $this->db->where('UserName' , $username);
        $this->db->update($this->dbn . '.Points', $data);
        return $this->db->affected_rows();
    }

    function insert_topup_record($data) {
        $this->db->insert($this->dbn . '.TopupRecord', $data);
        return $this->db->insert_id();
    }


    function insert_topup($data) {
        $this->db->insert($this->dbn . '.Topup', $data);
        return $this->db->insert_id();
    }

    function insert_itemshop_record($data) {
        $this->db->insert($this->dbn . '.ItemShopRecord', $data);
        return $this->db->insert_id();
    }

    function insert_covert_record($data) {
        $this->db->insert($this->dbn . '.ConvertRecord', $data);
        return $this->db->insert_id();
    }

    function get_record($table, $username) {
        return $this->db->get_where($this->dbn . $table, array("UserName" => $username))->result();
    }

    /*********************************************************************
    *   DO NOT TOUCH THIS. YOU'VE BEEN WARNED!                           *
    **********************************************************************/

    function get_setting($keyword) {
        return $this->db->get_where($this->dbn . '.Settings', array("keyword" => $keyword))->row();
    }

    function get_admin($username, $password) {
        $data = array(
            "username" => $username,
            "password" => $password
        );
        return $this->db->get_where($this->dbn . '.Users', $data)->row();
    }

    function get_admin_userid($userid) {
        return $this->db->get_where($this->dbn . '.Users', array('userid' => $userid))->row();
    }

    function insert_activity($data) {
        $this->db->insert($this->dbn . '.Activities', $data);
        return $this->db->insert_id();
    }

    function get_activities($limit = "5") {
        $this->db->limit($limit);
        $this->db->order_by('date_created', 'desc');
        return $this->db->get($this->dbn . '.Activities')->result();
    }

    function update_admin($username, $password, $data) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->update($this->dbn . '.Users', $data);
        return $this->db->affected_rows();
    }

    function insert_admin($data) {
        $this->db->insert($this->dbn . '.Users', $data);
        return $this->db->insert_id();
    }

    function insert_news($data) {
        $this->db->insert($this->dbn . '.News', $data);
        return $this->db->insert_id();
    }

    function update_news($newsid, $data) {
        $this->db->where('newsid', $newsid);
        $this->db->update($this->dbn . '.News', $data);
        return $this->db->affected_rows();
    }

    function delete_news($newsid) {
        $this->db->where('newsid', $newsid);
        $this->db->delete($this->dbn . '.News');
        return $this->db->affected_rows();
    }

    function get_news() {
        return $this->db->get($this->dbn . '.News')->result();
    }

    function get_news_front() {
        return $this->db->get_where($this->dbn . '.News', array("status" => 1))->result();
    }

    function get_news_id($newsid) {
        return $this->db->get_where($this->dbn . '.News', array("newsid" => $newsid))->row();
    }

}
