<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index() {
		$data['page_title'] = "Home";
		$this->load->view('home_view', $data);
	}

	public function register() {

		$this->form_validation->set_rules('username','Username','required|max_length[20]|min_length[6]|callback_check_username');
		$this->form_validation->set_rules('password','Password','required|max_length[25]|min_length[6]');
		$this->form_validation->set_rules('re_password','Re-Password','required|matches[password]');
		$this->form_validation->set_rules('pin','Pin Code','required|max_length[25]|min_length[6]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|max_length[50]|callback_check_email');
		$this->form_validation->set_rules('secret_question','Secret Question','required');
		$this->form_validation->set_rules('secret_answer','Secret Answer','required');
		$this->form_validation->set_rules('captcha','Captcha','required|callback_check_captcha');

		if($this->form_validation->run()) {
			$user_data = array(
				"UserName" => $this->input->post('username'),
				"UserID" => $this->input->post('username'),
				"UserPass" => $this->cf->hash($this->input->post('password')),
				"UserPass2" => $this->cf->hash($this->input->post('pin')),
				"UserEmail" => $this->input->post('email'),
				"UserSQ" => $this->input->post('secret_question'),
				"UserSA" => $this->input->post('secret_answer')
			);

			$points_data = array(
				"UserName" => $this->input->post('username'),
				"Points" => "0",
				"VPoints" => "0"
			);

			$this->CP_Model->insert_points($points_data);

			$u = $this->User_Model->insert_user($user_data);
			$this->session->set_flashdata('reg_err', "<div class='success-holder'><p>You have successfully registered. You can now use this account</p></div>");
			$this->session->set_userdata('UserName', $this->input->post('username'));
			$activity_data = array(
				"description" => "Account Registration",
				"userid" => $u,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			redirect(base_url() . 'user/');
		} else {
			$this->session->set_flashdata('reg_err', '<div class="error-holder">' . validation_errors('<p>', '</p>') . '</div>');
		}

		$this->load->library('image_lib');
		$this->load->helper('captcha');

		$vals = array(
		    'img_path'      => './assets/captcha/',
		    'img_url'       => base_url() . 'assets/captcha/',
        	'word_length'   => 4,
        	'pool'          => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'font_size'     => 24,
			'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 255)
        	)
		);

		$cap = create_captcha($vals);
		$this->session->set_userdata('captcha', $cap['word']);
		$data['cap'] = $cap['image'];
		$data['page_title'] = "Registration";
		$this->load->view('register_view', $data);
	}

	public function login() {

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_check_account');
		if($this->form_validation->run()) {
			$this->session->set_flashdata('log_error', "Welcome {$this->input->post('username')}!!");
		} else {
			$this->session->set_flashdata('log_error', '<div class="error-holder">' . validation_errors('<p>', '</p>') . '</div>');
		}

		if($this->session->userdata('UserName') !== null) {
			if($this->input->post('url') !== null) {
				redirect($this->input->post('url'));
			} else {
				redirect(base_url());
			}

		}

		$data['page_title'] = "Login";
		$this->load->view('login_view', $data);
	}

	public function rankings($sort = 'lvl', $server = '1', $class = "") {
		$valid = array('sw','gd','pk','lvl','cw');
		if($sort == 'top') {
				if($class != "") {
					$arr = "";
					switch($class) {
						case 'brawler': $arr = array(1,64); break;
						case 'swordsman': $arr = array(2,128); break;
						case 'archer': $arr = array(4,256); break;
						case 'shamman': $arr = array(8,512); break;

						default: $arr = array(1,64);
					}
					$data['players'] = $this->User_Model->get_ranking_top($server, 50 , $arr);
				} else {
					$data['players'] = $this->User_Model->get_ranking($server, 'lvl', 50);
				}
		} else {
			if(in_array($sort, $valid)) {
				$data['players'] = $this->User_Model->get_ranking($server, $sort, 50);
			} else {
				$data['players'] = $this->User_Model->get_ranking($server, 'lvl', 50);
			}
		}

		$data['server'] = $server;
		$data['class'] = '/'.$class;
		$data['page_title'] = "Ranking";
		$this->load->view('ranking_view', $data);
	}

	public function news($newid) {
		$data['news'] = $this->CP_Model->get_news_id($newid);
		$data['page_title'] = "News";
		$this->load->view('news_view', $data);
	}

	public function logout() {
		$activity_data = array(
			"description" => "Account Logout",
			"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
			"type" => 1
		);
		$this->CP_Model->insert_activity($activity_data);
		session_destroy();
		redirect(base_url());
	}

	public function forgotpw() {

		$this->form_validation->set_rules('email','Email','required|valid_email|callback_check_email_pw');
		if ($this->form_validation->run()) {
			$activity_data = array(
				"description" => "Forgotten Password",
				"userid" => $this->User_Model->get_username($this->input->post('username'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);
			$this->session->set_flashdata('pwerr', '<div class="success-holder">Please check your email we\'ve sent an temporary password</div>');
		} else {
			$this->session->set_flashdata('pwerr', '<div class="error-holder">' . validation_errors('<p>', '</p>') . '</div>');
		}

		$data['page_title'] = "Forgot Password";
		$this->load->view('forgot_password_view', $data);
	}

	public function check_email_pw($email) {
		$username = $this->input->post('username');
		if($this->User_Model->get_username($username) !== null) {
			if($this->User_Model->get_email($email) !== null) {
				$u = $this->User_Model->get_email($email);
				$temp_pass = "balik" . mt_rand(100000, 999999) . "cs";
				$user_data = array(
					"UserPass" => $this->cf->hash($temp_pass)
				);

				$this->load->library('email', $this->config->item('email')['default']);
				$this->email->set_newline("\r\n");
				$this->email->from($this->config->item('email')['default']['smtp_user'], 'BalikRan'); // change it to yours
				$this->email->to($email); // change it to yours
				$this->email->subject('Balik Ran Forgot Password');
				$this->email->message("Your temporary password is " . $temp_pass);
				$this->email->send();

				$this->User_Model->update_userinfo($u->UserName, $user_data);
				return true;
			} else {
				$this->form_validation->set_message('check_email_pw', "The Email {$email} doesn't exists.");
				return false;
			}
		} else {
			$this->form_validation->set_message('check_email_pw', "The Username {$username} doesn't exists.");
			return false;
		}
	}

	public function test() {
		$this->config->load('cs/config');
		echo $this->config->item('username');

	}

	/*
	-------------------------
		LOGIN FUNCTION
	--------------------------
		This process the user login.
	*/
	function check_account($password) {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if(($u = $this->User_Model->get_username($username)) !== null) {
			if($u->UserPass == $this->cf->hash($password)) {
				$activity_data = array(
					"description" => "Account Login",
					"userid" => $u->UserNum,
					"type" => 1
				);
				$this->CP_Model->insert_activity($activity_data);
				$this->session->set_userdata('UserName', $u->UserName);
				return true;
			} else {
				$this->form_validation->set_message('check_account', "Username and Password is doesn't match.");
				return false;
			}
		} else {
			$this->form_validation->set_message('check_account', "The Username {$username} doesn't exists.");
			return false;
		}
	}

	function check_username($username) {
		if($this->User_Model->get_username($username) == null) {
			return true;
		} else {
			$this->form_validation->set_message('check_username', "The Username {$username} already exists.");
			return false;
		}
	}

	function check_email($email) {
		if($this->User_Model->get_email($email) == null) {
			return true;
		} else {
			$this->form_validation->set_message('check_email', "The Email {$email} already exists.");
			return false;
		}
	}

	function check_captcha($c) {
		if(strtolower($c) == strtolower($this->session->userdata('captcha'))) {
			return true;
		} else {
			$this->form_validation->set_message('check_captcha', "Captcha doesn't match.");
			return false;
		}
	}

}
