<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {
		$user_data = array(
			"UserName" => "carlo2909",
			"UserID" => "carlo",
			"UserPass" => "carloflores",
			"UserPass2" => "carloflores2",
			"UserEmail" => "carlo29092@gmail.com"
		);

		#$this->User_Model->insert_user($user_data);
	}

	public function register() {

		$this->form_validation->set_rules('username','Username','required|max_length[20]|min_length[6]|callback_check_username');
		$this->form_validation->set_rules('password','Password','required|max_length[25]|min_length[6]');
		$this->form_validation->set_rules('password2','Password 2','required|max_length[25]|min_length[6]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|max_length[50]|callback_check_email');

		if($this->form_validation->run()) {
			$user_data = array(
				"UserName" => $this->input->post('username'),
				"UserID" => $this->input->post('userid'),
				"UserPass" => $this->cf->hash($this->input->post('password')),
				"UserPass2" => $this->cf->hash($this->input->post('password2')),
				"UserEmail" => $this->input->post('email')
			);

			$u = $this->User_Model->insert_user($user_data);
			if($u)
				$this->session->set_flashdata('error', "You have successfully registered. You can now use this account");
		}

		$this->load->view('test');
	}

	public function login() {

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_check_account');

		if($this->form_validation->run()) {
			$this->session->set_flashdata('error', "Welcome {$this->input->post('username')}!!");
		}

		if($this->session->userdata('UserName') !== null) {
			echo "Logged In!";
		} else {
			$this->load->view('test2');
		}


	}

	public function logout() {
		session_destroy();
		redirect(base_url() . 'welcome/login/');
	}

	/*
	-------------------------
		LOGIN FUNCTION
	--------------------------
		This process the user login.
	*/
	function check_account($password) {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if(($u = $this->User_Model->get_username($username)) !== null) {
			if($u->UserPass == $this->cf->hash($password)) {
				$this->session->set_userdata('UserName', $u->UserName);
				return true;
			} else {
				$this->form_validation->set_message('check_account', "Username and Password is doesn't match.");
				return false;
			}
		} else {
			$this->form_validation->set_message('check_account', "The Username {$username} doesn't exists.");
			return false;
		}
	}

	function check_username($username) {
		if($this->User_Model->get_username($username) == null) {
			return true;
		} else {
			$this->form_validation->set_message('check_username', "The Username {$username} already exists.");
			return false;
		}
	}

	function check_email($email) {
		if($this->User_Model->get_email($email) == null) {
			return true;
		} else {
			$this->form_validation->set_message('check_email', "The Email {$email} already exists.");
			return false;
		}
	}

}
