<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function index($sec = 'ep', $search = "") {

        $s = 1;
        if($sec == "ep") {
            $data['page_title'] = "Shop - E-Points";
            $s = 1;
        } else {
            $data['page_title'] = "Shop - V-Points";
            $s = 2;
        }

		if($this->input->post('search') !== null) {
			redirect(base_url(). 'shop/index/' . $sec . '/' . $this->input->post('search')  . '/');
		}

		$data['sec'] = $sec;

        if(!empty($search)) {
            $data['products'] = $this->Shop_Model->get_all_items($s);
        } else {
            $data['products'] = $this->Shop_Model->get_search($s, $search);
        }

		$this->load->view('shop/index_shop_view', $data);
	}

	public function ctg($sec = 'ep', $ctg) {
		if(empty($ctg)) {
			redirect(base_url(). 'shop/index/' . $sec . '/');
		}

		$s = 1;
		if($sec == "ep") {
			$data['page_title'] = "Shop - E-Points";
			$s = 1;
		} else {
			$data['page_title'] = "Shop - V-Points";
			$s = 2;
		}

		$data['sec'] = $sec;
		$data['products'] = $this->Shop_Model->get_all_items_ctg($ctg);

		$this->load->view('shop/index_shop_view', $data);
	}


    public function purchase($product_num, $sec) {
        if($this->session->userdata('UserName') === null) {
			redirect(base_url());
			exit();
		}

		$type = $sec == 1 ? "ep" : "vp";

        if($this->input->post('purchase') === null
        || $this->Shop_Model->get_item($product_num) === null) {
			$activity_data = array(
				"description" => "Trying to access purchase url without post request. Trying to find a hole.",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);

            $this->session->set_flashdata('err_pur', "<div class='error-holder'><p>Don't fck the system.</p></div>");
            redirect(base_url() . 'shop/index/' . $type);
            exit();
        }

        $username = $this->session->userdata('UserName');
        $p = $this->Shop_Model->get_item($product_num);
        $pnts = $this->CP_Model->get_points($username);

        $cVp = $pnts->VPoints;
        $cEp = $pnts->Points;



		if($sec == 1 && $cEp < $p->ItemPrice) {
			$this->session->set_flashdata('err_pur', "<div class='error-holder'><p>Doesn't have enough E Points to purchase.</p></div>");
            redirect(base_url() . 'shop/index/' . $type . '/');
			exit();
		}

		if($sec == 2 && $cVp < $p->ItemPrice) {
			$this->session->set_flashdata('err_pur', "<div class='error-holder'><p>Doesn't have enough Vote Points to purchase.</p></div>");
            redirect(base_url() . 'shop/index/' . $type . '/');
			exit();
		}

        if($p->Itemstock != 0 || $p->IsUnli) {

            if($p->IsUnli == 0) {
				$lItemStock = (int)$p->Itemstock - 1;
                $this->Shop_Model->update_itemmap(array("Itemstock" => $lItemStock), $product_num);
            }

            switch($p->ItemSec) {
                case 1:
                    $this->CP_Model->update_points(array("Points" => ($cEp - (int)$p->ItemPrice)), $username);
                    $t = "E-Points";
                break;

                case 2:
                    $this->CP_Model->update_points(array("VPoints" => ($cVp - (int)$p->ItemPrice)), $username);
                    $t = "V-Points";
                break;
            }

            $itemshop_rec_data = array(
                "UserName" => $username,
                "Item" => $p->ProductNum,
                "Price" => $p->ItemPrice,
                "OPoints" => $t == "E-Points" ? $cEp : $cVp,
                "NPoints" => $t == "E-Points" ? ($cEp - (int)$p->ItemPrice) : ($cVp - (int)$p->ItemPrice),
                "Status" => "Success",
                "Type" => $sec
            );
            $this->CP_Model->insert_itemshop_record($itemshop_rec_data);

            $pur_data = array(
                "UserUID" => $username,
                "ProductNum" => $product_num,
				"PurPrice" => $p->ItemPrice
            );
            $this->Shop_Model->insert_shoppurchase($pur_data);

			$activity_data = array(
				"description" => "Success Item Shop Purchase",
				"userid" => $this->User_Model->get_username($this->session->userdata('UserName'))->UserNum,
				"type" => 1
			);
			$this->CP_Model->insert_activity($activity_data);

            $this->session->set_flashdata('err_pur', "<div class='success-holder'><p>Successfully Purchased! Check your Item Bank.</p></div>");
            redirect(base_url() . 'shop/index/' . $type . '/');

        } else {
            $this->session->set_flashdata('err_pur', "<div class='error-holder'><p>Item doesn't have enough stock.</p></div>");
            redirect(base_url() . 'shop/index/' . $type . '/');
        }
    }

}
