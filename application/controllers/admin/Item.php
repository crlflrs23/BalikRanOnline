<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

    public function index() {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $data['page_title'] = "Item Shop List";
        $this->load->view('admin/all_items_view', $data);
    }

    public function add() {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $config['upload_path']          = './assets/img/itemshop/';
        $config['allowed_types']        = 'jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->form_validation->set_rules('main', 'Item Main', 'required');
        $this->form_validation->set_rules('sub', 'Item Sub', 'required');
        $this->form_validation->set_rules('sec', 'Item Sec', 'required|integer');
        $this->form_validation->set_rules('stock', 'Item Stock', 'required|integer');
        $this->form_validation->set_rules('ctg', 'Item Category', 'required|integer');
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        $this->form_validation->set_rules('name', 'Item Name', 'required');
        $this->form_validation->set_rules('price', 'Item Price', 'required|integer');
        $this->form_validation->set_rules('isunli', 'Is Unli', 'required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('ss', 'Item SS', '');

        if ($this->form_validation->run()) {
            $item_data = array();
            if(!empty($_FILES['userfile']['name']) && $this->upload->do_upload('ss')) {
                $upload_data = $this->upload->data();
                $item_data['ItemSS'] = $upload_data['file_name'];
            }

            $item_data['ItemMain'] = $this->input->post('main');
            $item_data['ItemSub'] = $this->input->post('sub');
            $item_data['ItemSec'] = $this->input->post('sec');
            $item_data['Itemstock'] = $this->input->post('stock');
            $item_data['ItemCtg'] = $this->input->post('ctg');
            $item_data['ItemName'] = $this->input->post('name');
            $item_data['ItemComment'] = $this->input->post('comment');
            $item_data['IsUnli'] = $this->input->post('isunli');
            $item_data['ItemPrice'] = $this->input->post('price');
            $item_data['hidden'] = $this->input->post('hid');

            $this->Shop_Model->insert_itemmap($item_data);

            $err_data = array(
                "status" => 1,
                "error" => "Successfull added new Item to Itemshop"
            );

            $this->session->set_flashdata('err', $err_data);

       } else {
           $err_data = array(
               "status" => 0,
               "error" => validation_errors('<p>', '</p>')
           );

           $this->session->set_flashdata('err', $err_data);
       }

       $data['page_title'] = "Add Item to Item Shop";
       $this->load->view('admin/add_item_view', $data);
    }

    public function edit($ProductNum) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $config['upload_path']          = './assets/img/itemshop/';
        $config['allowed_types']        = 'jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $this->form_validation->set_rules('main', 'Item Main', 'required');
        $this->form_validation->set_rules('sub', 'Item Sub', 'required');
        $this->form_validation->set_rules('sec', 'Item Sec', 'required|integer');
        $this->form_validation->set_rules('stock', 'Item Stock', 'required|integer');
        $this->form_validation->set_rules('ctg', 'Item Category', 'required|integer');
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        $this->form_validation->set_rules('name', 'Item Name', 'required');
        $this->form_validation->set_rules('price', 'Item Price', 'required|integer');
        $this->form_validation->set_rules('isunli', 'Is Unli', 'required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('ss', 'Item SS', '');

        if ($this->form_validation->run()) {
            $item_data = array();
            if(!empty($_FILES['userfile']['name']) && $this->upload->do_upload('ss')) {
                $upload_data = $this->upload->data();
                $item_data['ItemSS'] = $upload_data['file_name'];
            }

            $item_data['ItemMain'] = $this->input->post('main');
            $item_data['ItemSub'] = $this->input->post('sub');
            $item_data['ItemSec'] = $this->input->post('sec');
            $item_data['Itemstock'] = $this->input->post('stock');
            $item_data['ItemCtg'] = $this->input->post('ctg');
            $item_data['ItemName'] = $this->input->post('name');
            $item_data['ItemComment'] = $this->input->post('comment');
            $item_data['IsUnli'] = $this->input->post('isunli');
            $item_data['ItemPrice'] = $this->input->post('price');
            $item_data['hidden'] = $this->input->post('hid');

            $this->Shop_Model->update_itemmap($item_data, $ProductNum);

            $err_data = array(
                "status" => 1,
                "error" => "Successfully Updated Item #" . $ProductNum
            );

            $this->session->set_flashdata('err', $err_data);

       } else {
           $err_data = array(
               "status" => 0,
               "error" => validation_errors('<p>', '</p>')
           );

           $this->session->set_flashdata('err', $err_data);
       }

       $data['p'] = $this->Shop_Model->get_item($ProductNum);
       $data['page_title'] = "Add Item to Item Shop";
       $this->load->view('admin/edit_item_view', $data);
    }

    public function delete($ProductNum) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->Shop_Model->delete_itemmap($ProductNum);
        $err_data = array(
            "status" => 1,
            "error" => "Successfully Deleted Item #" . $ProductNum
        );
        $this->session->set_flashdata('err', $err_data);

        redirect(base_url() . 'admin/item/');
    }

}
