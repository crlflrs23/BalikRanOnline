<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Points extends CI_Controller {

    public function index() {

        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username');
        if($this->form_validation->run()) {
            redirect(base_url() . 'admin/points/characters/' . $this->input->post('username') . '/');
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $data['page_title'] = "Points | Character Finder";
        $this->load->view('admin/find_character_view', $data);

    }

    function check_username($username) {
        if($this->User_Model->get_username($username) !== null) {
            return true;
        } else {
            $this->form_validation->set_message('check_username', 'Username doesn\'t Exist.');
            return false;
        }
    }

    public function characters($username) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $u = $this->User_Model->get_username($username);
        $data['char'] = $this->Game_Model->get_characters($u->UserNum);
        $data['u'] = $u;
        $data['page_title'] = "Points | Character Finder";
        $this->load->view('admin/all_character_view', $data);

    }

    public function info($username, $ChaNum) {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $this->form_validation->set_rules('name', 'IGN', 'required');
        $this->form_validation->set_rules('school', 'School', 'required');
        $this->form_validation->set_rules('stro', 'Strong', 'required');
        $this->form_validation->set_rules('str', 'STR', 'required');
        $this->form_validation->set_rules('dex', 'DEX', 'required');
        $this->form_validation->set_rules('spi', 'SPI', 'required');
        $this->form_validation->set_rules('pow', 'Pow', 'required');
        $this->form_validation->set_rules('int', 'Int', 'required');
        $this->form_validation->set_rules('ep', 'E-Points', 'required');
        $this->form_validation->set_rules('vp', 'Vote Points', 'required');

        if($this->form_validation->run()) {
            $char_data = array(
                "ChaName" => $this->input->post('name'),
                "ChaSchool" => $this->input->post('school'),
                "ChaStrong" => $this->input->post('stro'),
                "ChaStrength" => $this->input->post('str'),
                "ChaDex" => $this->input->post('dex'),
                "ChaSpirit" => $this->input->post('spi'),
                "ChaPower" => $this->input->post('pow'),
                "ChaIntel" => $this->input->post('int')
            );

            $this->Game_Model->update_chainfo($ChaNum, $char_data);

            $points_data = array(
                "Points" => $this->input->post('ep'),
                "VPoints" => $this->input->post('vp'),
            );

            $this->CP_Model->update_points($points_data, $username);
            $err_data = array(
                "status" => 1,
                "error" => "Successfully Updated #" . $ChaNum
            );

            $this->session->set_flashdata('err', $err_data);
        } else {
            $err_data = array(
                "status" => 0,
                "error" => validation_errors('<p>', '</p>')
            );

            $this->session->set_flashdata('err', $err_data);
        }

        $u = $this->Game_Model->ChaInfo($ChaNum);
        $data['p'] = $this->CP_Model->get_points($username);
        $data['c'] = $u;
        $data['user'] = $username;
        $data['page_title'] = "Character Information";
        $this->load->view('admin/info_character_view', $data);

    }


}
