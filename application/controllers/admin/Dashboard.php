<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index() {
        if($this->session->userdata('admin') == null) {
            redirect(base_url() . 'admin/');
            exit();
        }

        $data['page_title'] = "Dashboard";
        $this->load->view('admin/dashboard_view', $data);
    }

}
