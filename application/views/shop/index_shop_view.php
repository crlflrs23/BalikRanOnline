<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>


    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">

            <div class="panel-container">
                <div class="panel-header">
                    <h1>Vote Shop Message</h1>
                </div>

                <div class="panel-content">
                    <center>
                        <?=$this->config->item('vs_message')?>
                    </center>
                </div>
            </div>

            <?php if ($this->session->flashdata('err_pur') !== null): ?>
                <div class="panel-container">
                    <div class="panel-header">
                        <h1>Message</h1>
                    </div>

                    <div class="panel-content">
                        <center>
                            <?=$this->session->flashdata('err_pur')?>
                        </center>
                    </div>
                </div>
            <?php endif; ?>

            <div class="panel-container">
                <div class="panel-header">
                    <h1>Items</h1>
                </div>

                <div class="panel-content">
                    <center>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/1/">
                                Enchancements
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/2/">
                                EXP Rosaries
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/3/">
                                Destiny Boxes
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/4/">
                                Pet System
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/5/">
                                Accesories
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/6/">
                                Miscellaneous
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/7/">
                                Weapons
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/8/">
                                Costumes
                            </a>
                        </span>
                        <span class="option separate">/</span>
                        <span class="option">
                            <a href="<?=base_url()?>shop/ctg/<?=$sec?>/9/">
                                Potions
                            </a>
                        </span>
                    </center>

                    <br/>
                    <!-- Item List Starts Here -->

                    <center>
                        <?php if ($products): ?>
                            <div class="shop-listing">
                            <?php foreach ($products as $key => $var): ?>
                                <?php $img = $var->ItemSS == null ? 'default.jpg' : $var->ItemSS; ?>

                                    <div class="col_3">
                                        <img src="<?=base_url()?>assets/img/itemshop/<?=$img?>" alt="">
                                        <h2 class="item-name" style="margin:0;"><?=$var->ItemName?></h2>
                                        <small><?=$var->ItemComment?><br/><b><?=$var->ItemPrice?> <?=$var->ItemSec == 1 ? $this->config->item('point_name') : $this->config->item('vpoint_name')?></b> <b><?=$var->Itemstock == 0 && $var->IsUnli == 0 ? " - Out of Stock" : ""?></b></small><br/>
                                        <?php if ($this->session->userdata('UserName') !== null && $var->Itemstock != 0): ?>
                                            <form action="<?=base_url()?>shop/purchase/<?=$var->ProductNum?>/<?=$var->ItemSec?>/" method="post">
                                                <input type="submit" name="purchase" style="padding: 5px !important;" value="Purchase">
                                            </form>
                                        <?php endif; ?>
                                    </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            No Item Available.
                        <?php endif; ?>

                    <div class="clearfix"></div>
                        </div>
                    </center>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <?php $this->load->view('mod/panel_shop_search_view')?>
            <?php $this->load->view('mod/panel_login_view') ?>
            <?php $this->load->view('mod/panel_ranking_view') ?>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {

        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>

</html>
