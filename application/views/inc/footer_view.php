<div class="container_12">
    <footer>
        <div class="grid_4" style="padding-top: 15px;">
            Copy Right 2016 All Rights Reserved
        </div>

        <div class="push_5 grid_2">
            <img src="<?=base_url()?>assets/img/logo-footer.png" width="120" alt="">
        </div>
        <div class="push_5 grid_2">
            <img src="<?=base_url()?>assets/img/cs-logo.png" data-checker="59eb7c8383891dbfa38f76d332962b70" width="40" alt="">
        </div>

        <div class="clearfix"></div>
    </footer>
</div>

<style>
    .error-holder {
        background: rgba(182, 21, 21, .7);
        color: #fff;
        width: calc(100% - 60px);
        height: auto;
        border-radius: 10px;
        margin-bottom: 20px;
    }

    .error-holder p {
        margin: 0;
        font-size: 12px;
        padding: 5px;
    }

    .success-holder {
        background: rgba(21, 182, 43, .7);
        color: #fff;
        width: calc(100% - 60px);
        height: auto;
        border-radius: 10px;
        margin-bottom: 20px;
    }

    .success-holder p {
        margin: 0;
        font-size: 12px;
        padding: 5px;
    }
</style>
