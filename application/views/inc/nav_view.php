<body>
    <div class="container_12">
        <nav>
            <center>
                <ul class="main-menu">
                    <li class="main-menu-item <?=$this->uri->segment(2) == "" ? 'active' : ''?>">
                        <a href="<?=base_url()?>">
                            Home
                        </a>
                    </li>
                    <li class="main-menu-item  <?=$this->uri->segment(2) == "rankings" ? 'active' : ''?>">
                        <a href="<?=base_url()?>home/rankings/">
                            Rankings
                        </a>
                    </li>
                    <li class="main-menu-item  <?=$this->uri->segment(3) == "vp" ? 'active' : ''?>">
                        <a href="<?=base_url()?>shop/index/vp/">
                            Vote Shop
                        </a>
                    </li>
                    <li class="main-menu-item  <?=$this->uri->segment(3) == "ep" ? 'active' : ''?>">
                        <a href="<?=base_url()?>shop/index/ep/">
                            Premium Shop
                        </a>
                    </li>
                    <li class="main-menu-item  <?=$this->uri->segment(1) == "downloads" ? 'active' : ''?>">
                        <a href="<?=base_url()?>home/download/">
                            Downloads
                        </a>
                    </li>
                    <li class="main-menu-item">
                        <a href="#">
                            Group
                        </a>
                    </li>
                </ul>
            </center>
        </nav>
    </div>

    <div class="container_12">
        <center>
            <img src="<?=base_url()?>assets/img/logo.png" class="logo" alt="">
        </center>
    </div>
