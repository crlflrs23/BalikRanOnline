<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Convert <?=$this->config->item('point_name')?> to <?=$this->config->item('vpoint_name')?></h1>
                </div>

                <div class="panel-content">


                    <form action="<?=base_url()?>user/eptvp/" method="post">
                        <div class="form-group">
                            <p>
                                1 <?=$this->config->item('point_name')?> = <?=$this->config->item('ep_to_vp')?> <?=$this->config->item('vpoint_name')?>
                            </p>

                            <p>
                                You Currently have  <span class="label bolder red"><?=$this->CP_Model->get_points($this->session->userdata['UserName'])->Points?></span> <?=$this->config->item('point_name')?>
                            </p>

                            <?=$this->session->flashdata('eptvp_err')?>

                            <div class="push_2 col_2">
                                <label for="points"><?=$this->config->item('point_name')?> to be Converted</label>
                                <input id="points" name="epoints" class="full" type="number" min="20" placeholder="How many <?=$this->config->item('point_name')?>?">
                            </div>
                        </div>

                        <Br/>
                        <center>
                            <input type="submit" value="Convert">
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
