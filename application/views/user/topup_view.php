<?php
    $this->load->view('inc/head_view');
    $this->load->view('inc/nav_view');
?>

    <style media="screen">
        .link {
            color: #ff5e00;
        }
    </style>

    <!-- Main Content -->
    <div class="container_12">
        <!-- Content -->
        <div class="grid_8">
            <div class="panel-container">
                <div class="panel-header">
                    <h1>Topup</h1>
                </div>

                <div class="panel-content">


                    <form action="<?=base_url()?>user/topup/" method="post">
                        <div class="form-group">
                            <p>
                                Please go to <a class="link" href="#">Donate Guide</a> to know on how to obtain a top up code.
                            </p>

                            <?=$this->session->flashdata('top_err')?>

                            <div class="col_2">
                                <label for="username">Card Code</label>
                                <input id="username" name="code" class="full" type="text" placeholder="Card Code">
                            </div>

                            <div class="col_2">
                                <label for="username">Card Pin Code</label>
                                <input id="username" name="pincode" class="full" type="text" placeholder="Pin Code">
                            </div>
                        </div>
                        <Br/>
                        <center>
                            <input type="submit" value="Topup">
                        </center>
                    </form>
                </div>
            </div>
        </div>

        <!-- Side Bar -->
        <div class="grid_4">
            <!-- login -->
            <div class="grid_4">
                <!-- login -->
                <?php $this->load->view('mod/panel_login_view') ?>
                <?php $this->load->view('mod/panel_ranking_view') ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('inc/footer_view') ?>

</body>
<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(function() {
        // Tab-Pane
        $('a[data-activate-id]').click(function() {
            var id = $(this).attr('data-activate-id');
            var type = $(this).attr('data-tab-type');



            $(".tab-pane[id!='"+id+"'][data-tab-type='"+type+"']").fadeOut('fast');
            $("#"+ id +"").delay(200).fadeIn('slow');

            return false;
        });
    });
</script>
</html>
