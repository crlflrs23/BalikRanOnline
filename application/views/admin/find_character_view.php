<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="eight columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Character Finder</h3>
                            <p class="panel-sub">This tool will be used to get all the information of a particular character.</p>
                        </div>

                        <div class="panel-content">
                            <?php if ($this->session->flashdata('err') !== null): ?>
                                <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                    <?=$this->session->flashdata('err')['error']?>
                                </div>
                            <?php endif; ?>

                            <div class="centered eight columns">
                                <form action="<?=base_url()?>admin/points/" method="post" class="field">
                                    <input type="text" name="username" placeholder="Username" class="input">
                                    <input type="submit" value="Find" class="medium success btn">
                                </form>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
