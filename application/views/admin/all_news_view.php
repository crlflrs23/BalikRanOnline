<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">News List</h3>
                            <p class="panel-sub">List of all news created.</p>
                        </div>

                        <div class="panel-content">
                            <?php if ($this->session->flashdata('err') !== null): ?>
                                <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                    <?=$this->session->flashdata('err')['error']?>
                                </div>
                            <?php endif; ?>
                            <table id="topup">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>User</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Date Created</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>User</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Date Created</th>
                                        <th>Tools</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($this->CP_Model->get_news() as $key => $var): ?>
                                        <?php
                                            switch($var->type) {
                                                case 1: $type = "<span class='danger label'>Promo</span>"; break;
                                                case 2: $type = "<span class='default label'>Rankings</span>"; break;
                                                case 3: $type = "<span class='success label'>Guides</span>"; break;
                                                case 4: $type = "<span class='warning label'>System</span>"; break;
                                                case 5: $type = "<span class='primary label'>News</span>"; break;
                                            }
                                         ?>
                                        <tr>
                                            <td><?=$var->title?></td>
                                            <td><?=$this->CP_Model->get_admin_userid($var->userid)->username?></td>
                                            <td><?=$type?></td>
                                            <td><?=$var->status == 1 ? 'Published' : 'Draft'?></td>
                                            <td><?=$var->date_created?></td>
                                            <td>
                                                <a href="<?=base_url()?>admin/news/edit/<?=$var->newsid?>/"><button class="info label">Edit</button></a>
                                                <a href="<?=base_url()?>admin/news/delete/<?=$var->newsid?>/"><button class="danger label" onclick="return confirm('Are you sure?')">Delete</button></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
    <script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" charset="utf-8"></script>
    <script type="text/javascript">
       $(document).ready(function(){
           $('#topup').DataTable();
       });
   </script>

</html>
