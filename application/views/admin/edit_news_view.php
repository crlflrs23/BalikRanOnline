<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Edit News<h3>
                            <p class="panel-sub">Edit a composed news.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="centered nine columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="" action="<?=base_url()?>admin/news/edit/<?=$news->newsid?>/" method="post">
                                        <div class="field">
                                            <div class="row">
                                                <div class="eight columns">
                                                    <input type="text" value="<?=$news->title?>" class="input" name="title" placeholder="Title">
                                                </div>
                                                <div class="four columns picker">
                                                    <select name="type">
                                                        <option <?=$news->type == 1 ? 'selected' : ''?> value="1">Promotion</option>
                                                        <option <?=$news->type == 2 ? 'selected' : ''?> value="2">Rankings</option>
                                                        <option <?=$news->type == 3 ? 'selected' : ''?> value="3">Guides</option>
                                                        <option <?=$news->type == 4 ? 'selected' : ''?> value="4">System</option>
                                                        <option <?=$news->type == 5 ? 'selected' : ''?> value="5">News</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <textarea name="description" class="input textarea" style="height: 400px;" placeholder="Content"><?=$news->description?></textarea>
                                            <input type="text" class="input" name="tags" placeholder="Tags (Ex. tag1,tag2,tag3)" value="<?=$news->tags?>">
                                            <div class="row">
                                                <div class="six columns"><input type="submit" name="publish" class="medium secondary btn" value="Publish News" /></div>
                                                <div class="six columns"><input type="submit" name="draft" class="medium warning btn" value="Save as Draft" /></div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

</html>
