<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Characters List of <?=$u->UserName?></h3>
                            <p class="panel-sub">List of all the characters of <?=$u->UserName?>.</p>
                        </div>

                        <div class="panel-content">
                            <?php if ($this->session->flashdata('err') !== null): ?>
                                <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                    <?=$this->session->flashdata('err')['error']?>
                                </div>
                            <?php endif; ?>
                            <table id="topup">
                                <thead>
                                    <tr>
                                        <th>Character</th>
                                        <th>Level</th>
                                        <th>Tools</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Character</th>
                                        <th>Level</th>
                                        <th>Tools</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($char as $key => $var): ?>
                                        <tr>
                                            <td><?=$var->ChaName?></td>
                                            <td><?=$var->ChaLevel?></td>
                                            <td>
                                                <a href="<?=base_url()?>admin/points/info/<?=$u->UserName?>/<?=$var->ChaNum?>/"><button class="info label">View Info</button></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
    <script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" charset="utf-8"></script>
    <script type="text/javascript">
       $(document).ready(function(){
           $('#topup').DataTable();
       });
   </script>

</html>
