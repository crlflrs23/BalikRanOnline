<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="twelve columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">Generate Top Up<h3>
                            <p class="panel-sub">Generate Top up for the player.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="five columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="" action="<?=base_url()?>admin/topup/generate/" method="post">
                                        <div class="field">
                                            <input type="text" class="input" name="code" placeholder="Code">
                                            <input type="text" class="input" name="pin" placeholder="Pin">
                                            <input type="number" min="0" class="input" name="amount" placeholder="Amount">
                                            <input type="submit" class="medium secondary btn" value="Generate" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
