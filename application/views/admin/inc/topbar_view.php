<nav class="topbar">
    <div class="row">
        <div class="three columns logo-holder">
            <img src="<?=base_url()?>res/img/cs-logo.png" alt="" />
            <span><i class="mob-menu icon-menu"> </i></span>
        </div>

        <div class="eight columns notif-holder">
            <div class="row">
                <div class="six columns">
                    <h5><?=$page_title?></h5>
                </div>
            <!--
            <div class="six columns">
                <div class="notifications pull_right">
                    <div>
                        <i class="noti-icon icon-search"> </i>
                    </div>

                    <div>
                        <span class="notify">9+</span>
                        <i class="noti-icon icon-layout"> </i>
                    </div>

                    <div>
                        <span class="notify">9+</span>
                        <i class="noti-icon icon-globe"> </i>
                    </div>
                </div>
            </div>
            -->
            </div>
        </div>
    </div>
</nav>
