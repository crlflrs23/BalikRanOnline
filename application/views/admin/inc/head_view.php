<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Panel - <?=$page_title?></title>
        <link rel="stylesheet" href="<?=base_url()?>res/css/gumby.css" media="screen">
        <link rel="stylesheet" href="<?=base_url()?>res/css/cs.main.min.css" media="screen">
        <script src="<?=base_url()?>res/js/libs/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
