<?php
    $s = $this->session->userdata('admin');
    $u = $this->CP_Model->get_admin($s['u'], $this->cf->hash($s['p']));
?>

<style>
    .sub-menu li a,
    .menu-item a {
        color: inherit;
    }
</style>

<div class="row">
    <div class="three columns">
        <div class="sidebar-menu-holder pinned">
            <nav class="sidebar-menu">
                <div class="user-holder">
                    <table>
                        <tr>
                            <td><div class="user-dp"></div></td>
                            <td>
                                <div class="user-name"><?=$u->fname . ' ' . $u->lname?></div>
                                <div class="user-email"><?=$u->email?></div>
                            </td>
                        </tr>
                    </table>
                    <center>
                        <table>
                            <tr>
                                <td><a href="<?=base_url()?>admin/user/"><i class="user-links icon-user"></i></a></td>
                                <td><a href="<?=base_url()?>admin/login/logout/"><i class="user-links icon-logout"></i></a></td>
                            </tr>
                        </table>
                    </center>
                </div>
                <hr>

                <div class="menu-container">
                    <ul class="menu-holder">
                        <li class="menu-label">Menu</li>
                        <li class="menu-item <?=$this->uri->segment(2) == "dash" ? 'active' : ''?>"><a href="<?=base_url()?>admin/dashboard/"><i class="icon-gauge"> </i> Dashboard</a></li>

                        <li class="menu-item <?=$this->uri->segment(2) == "topup" ? 'active' : ''?>">
                            <i class="icon-ticket"> </i> Top Up

                            <ul class="sub-menu <?=$this->uri->segment(2) == "topup" ? 'show' : 'hide'?>">
                                <li><a href="<?=base_url()?>admin/topup/">All Top Ups</a></li>
                                <li><a href="<?=base_url()?>admin/topup/generate/">Generate Top Up</a></li>
                            </ul>
                        </li>

                        <li class="menu-item <?=$this->uri->segment(2) == "news" ? 'active' : ''?>">
                            <i class="icon-book-open"> </i> News Management
                            <ul class="sub-menu <?=$this->uri->segment(2) == "news" ? 'show' : 'hide'?>">
                                <li><a href="<?=base_url()?>admin/news/">News List</a></li>
                                <li><a href="<?=base_url()?>admin/news/add/">Create New News</a></li>
                            </ul>
                        </li>

                        <li class="menu-item <?=$this->uri->segment(2) == "item" ? 'active' : ''?>">
                            <i class="icon-basket"> </i> Item Shop Management
                            <ul class="sub-menu <?=$this->uri->segment(2) == "item" ? 'show' : 'hide'?>">
                                <li><a href="<?=base_url()?>admin/item/">Item Shop List</a></li>
                                <li><a href="<?=base_url()?>admin/item/add/">Add Item</a></li>
                            </ul>
                        </li>

                        <li class="menu-item <?=$this->uri->segment(2) == "points" ? 'active' : ''?>">
                            <a href="<?=base_url()?>admin/points/"><i class="icon-upload"> </i> Find Character</a>
                        </li>

                        <li class="menu-item <?=$this->uri->segment(2) == "user" ? 'active' : ''?>">
                            <a href="<?=base_url()?>admin/user/create/"><i class="icon-users"> </i> Create User</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
