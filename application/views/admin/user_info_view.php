<?php $this->load->view('admin/inc/head_view') ?>
<?php $this->load->view('admin/inc/topbar_view') ?>
<?php $this->load->view('admin/inc/nav_view') ?>


        <div class="nine columns">
            <div class="row main-content">
                <div class="centered six columns">
                    <div class="panel">
                        <div class="panel-header">
                            <h3 class="panel-title">User Info<h3>
                            <p class="panel-sub">Edit User Information.</p>
                        </div>

                        <div class="panel-content">
                            <div class="row">
                                <div class="twelve columns">
                                    <?php if ($this->session->flashdata('err') !== null): ?>
                                        <div class="<?=$this->session->flashdata('err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                            <?=$this->session->flashdata('err')['error']?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="field" action="<?=base_url()?>admin/user/" method="post">
                                        <div class="row">
                                            <div class="six columns">
                                                <small>FIRST NAME</small>
                                                <input type="text" class="input" name="fname" value="<?=$u->fname?>" placeholder="First Name">
                                            </div>
                                            <div class="six columns">
                                                <small>LAST NAME</small>
                                                <input type="text" class="input" name="lname" value="<?=$u->lname?>" placeholder="Last Name">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <small>E-MAIL</small>
                                            <input type="text" class="input" name="email" value="<?=$u->email?>" placeholder="E-mail">
                                        </div>

                                        <small>CHANGE PASSWORD</small>
                                        <div class="row">
                                            <div class="four columns">
                                                <small>OLD PASSWORD</small>
                                                <input type="password" class="input" name="opass" placeholder="Old Password">
                                            </div>
                                            <div class="four columns">
                                                <small>NEW PASSWORD</small>
                                                <input type="password" class="input" name="npass" placeholder="New Password">
                                            </div>
                                            <div class="four columns">
                                                <small>RE NEW PASSWORD</small>
                                                <input type="password" class="input" name="rnpass" placeholder="Re New Password">
                                            </div>
                                        </div>

                                        <center>
                                            <input type="submit" value="Update Info" class="medium success btn">
                                        </center>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="screen" title="no title" charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>

</html>
