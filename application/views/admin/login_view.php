<?php $this->load->view('admin/inc/head_view') ?>

        <div class="row">
            <br/><br/><br/><br/>
            <div class="centered three columns">
                <div class="panel">
                    <div class="panel-header" style="background: #333b43; color: #fff; font-weight: 800;">
                        Admin Panel
                    </div>

                    <div class="panel-content">
                        <?php if ($this->session->flashdata('log_err') !== null): ?>
                            <div class="<?=$this->session->flashdata('log_err')['status'] == 0 ? 'danger' : 'success'?> alert" style="font-size: 10px !important;">
                                <?=$this->session->flashdata('log_err')['error']?>
                            </div>
                        <?php endif; ?>
                        <form class="field" action="<?=base_url()?>admin/login/" method="post">
                            <input type="text" style="margin-bottom: 5px;" name="username" class="input" placeholder="Username" />
                            <input type="password" style="margin-bottom: 5px;" name="password" class="input" placeholder="Password" />
                            <center>
                                <input type="submit" style="font-weight: 800; color: #fff;" class="medium info btn" value="LOGIN">
                            </center>
                        </form>
                    </div>
                </div>

                <center>
                    <img src="<?=base_url()?>assets/img/cs-logo.png" alt="">
                </center>
            </div>
        </div>

    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?=base_url()?>res/js/cs.common.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.min.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/cs.demo.chart.js" charset="utf-8"></script>
    <script src="<?=base_url()?>res/js/libs/gumby.min.js" charset="utf-8"></script>
</html>
